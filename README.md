# Thermosensorik Heidenheim


# Beschreibung:


Um die Einsatzleitung des Winterdienstes zu Unterstützen, hat die Stadtverwaltung Heidenheim gemeinsam mit der Fa.Elbesoft GmbH auf der Basis von Thingsboard ein Dashboard entwickelt.
Auf diesem Dashboard wird die Straßenbelagsoberflächentemperatur dargestellt, welche im 15 min. Zyklus von den Bussen der Heidenheimer Verkehrsgesellschaft gemessen wird.
Die Messwerte werden mit dem Temperaturwert, dem Zeitstempel sowie der entsprechenden Geokkoordinate an einen Server übermittelt und auf dem Thingsboard Dashboard dargestellt.



# Link zu Github

https://github.com/thingsboard/thingsboard.git


# Templates für Thingsboard

bus_entity_device.json: 
Device Profil - hier wird auch die Alarmierung der gemessenen Schwellwerte definiert.

thermosensorik_heidenheim.json:
Dashboard für die Visualisierung.

wetterdaten_rule_chain.json:
Auf dem Dashboard werden externe Wetterdaten über die openweather.api integriert. Hierfür wird eine zusätzliche Regelkette (Rule-Chain) verwendet. Die Rule-Chain kann mit dem File wetterdaten_rule_chain.json importiert werden.


## License
Apache-2.0
